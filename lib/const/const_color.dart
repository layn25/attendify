// ignore_for_file: non_constant_identifier_names, constant_identifier_names, slash_for_doc_comments

import 'package:flutter/material.dart';

/**
 * Primary : warna utama yang digunakan pada app (appbar, button, dll)
 * Background : warna untuk background screen (splash, fullbackgroundcolor)
 * Complementary: warna komplementer yang selaras dengan primary dan background
 * Success: warna untuk menandakan berhasil (popup, button)
 * Gradient: kombinasi warna transisi
 * --------------------------------------------------------------------------------
 * Cara Pemakaian:
 * 1. import file const_color.dart di halaman yang akan digunakan
 * 2. ketikan nama const pada parameter color -->
 *    Text('Test', style: TextStyle(color: PRIMARY_COLOR,)),
 */

const PRIMARY_COLOR = Color.fromARGB(255, 0, 183, 175);
final BACKGROUND_COLOR = Colors.teal.shade600;
const SECONDARY_COLOR = Color.fromARGB(255, 0, 0, 0);
const COMPLEMENTARY_COLOR1 = Color.fromARGB(255, 215, 239, 218);
const COMPLEMENTARY_COLOR2 = Color.fromARGB(255, 253, 212, 1);
const COMPLEMENTARY_COLOR3 = Color.fromARGB(255, 244, 189, 38);
const COMPLEMENTARY_COLOR4 = Color.fromARGB(255, 38, 158, 244);
const COMPLEMENTARY_COLOR5 = Color.fromARGB(255, 232, 29, 29);
const SUCCESS_COLOR = Color.fromARGB(255, 10, 137, 52);
const FAILED_COLOR = Color.fromARGB(255, 194, 14, 14);
const WARNING_COLOR = Color.fromARGB(255, 255, 206, 12);
const COMPLEMENTARY_COLOR6 = Color(0xFF473ED7);
const WHITE_COLOR = Color.fromARGB(255, 255, 255, 255);
const TRANSPARENT_COLOR = Colors.transparent;
const ABU_COLOR = Colors.grey;
const QR_COLOR = Color(0xFF3E3595);

LinearGradient gradientRed = const LinearGradient(
  colors: [Colors.red, Color.fromARGB(255, 112, 29, 3)],
  begin: Alignment.topLeft,
  end: Alignment.bottomRight,
);
LinearGradient gradientTeal = const LinearGradient(
  colors: [Color.fromARGB(255, 17, 193, 146), Color.fromARGB(255, 3, 112, 94)],
  begin: Alignment.topLeft,
  end: Alignment.bottomRight,
);
