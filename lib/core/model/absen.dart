class Absen {
  final int id;
  final int user_id;
  final int pertemuan_id;
  final String hadir;

  Absen(this.id, this.user_id, this.pertemuan_id, this.hadir);
}
