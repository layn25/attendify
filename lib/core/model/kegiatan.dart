import 'package:dio/dio.dart';

class Kegiatan {
  final int id;
  final String name;
  final int user_id;
  final String status;

  Kegiatan(this.id, this.name, this.user_id, this.status);
}

class KegiatanList {
  final Dio _dio = Dio();
  final String baseUrl = "https://attendify.tech/mobile/api";

  Stream<List<Kegiatan>> fetchKegiatan() async* {
    try {
      final response = await _dio.get('$baseUrl/kegiatan');
      if (response.statusCode == 200) {
        final List<Kegiatan> kegiatanList = (response.data['data'] as List)
            .map((item) =>
                Kegiatan(item['id'], item['name'], item['user_id'], item['status']))
            .toList();
        yield kegiatanList;
      } else {
        throw Exception('Failed to load data from API');
      }
    } catch (e) {
      throw Exception('Error: $e');
    }
  }

  where(Function(dynamic kegiatan) param0) {}

}