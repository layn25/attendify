import 'package:dio/dio.dart';

class User {
  final int id;
  final String name;
  final String nim;
  final String email;
  final String password;
  final String whatsapp;
  final String is_admin;

  User(this.id, this.name, this.nim, this.email, this.password, this.whatsapp, this.is_admin);
}

class UserList {
  final Dio _dio = Dio();
  final String baseUrl = "https://attendify.tech/mobile/api";

  Stream<List<User>> fetchUser() async* {
    try {
      final response = await _dio.get('$baseUrl/akun');
      if (response.statusCode == 200) {
        final List<User> userList = (response.data['data'] as List)
            .map((item) =>
                User(item['id'], item['name'], item['nim'], item['email'],item['password'], item['whatsapp'], item['is_admin']))
            .toList();
        yield userList;
      } else {
        throw Exception('Failed to load data from API');
      }
    } catch (e) {
      throw Exception('Error: $e');
    }
  }

}