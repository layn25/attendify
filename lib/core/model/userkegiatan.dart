import 'package:dio/dio.dart';

class UserKegiatan {
  final int id;
  final int user_id;
  final int kegiatan_id;

  UserKegiatan(this.id, this.user_id, this.kegiatan_id);
}

class UserKegiatanList {
  final Dio _dio = Dio();
  final String baseUrl = "https://attendify.tech/mobile/api";

  Stream<List<UserKegiatan>> fetchUserKegiatan() async* {
    try {
      final response = await _dio.get('$baseUrl/userkegiatan');
      if (response.statusCode == 200) {
        final List<UserKegiatan> kegiatanList = (response.data['data'] as List)
            .map((item) =>
                UserKegiatan(item['id'], item['user_id'], item['kegiatan_id']))
            .toList();
        yield kegiatanList;
      } else {
        throw Exception('Failed to load data from API');
      }
    } catch (e) {
      throw Exception('Error: $e');
    }
  }

}