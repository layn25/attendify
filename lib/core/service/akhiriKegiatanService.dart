import 'dart:convert';

import 'package:attendify/core/service/loginService.dart';
import 'package:attendify/view/admin/home/homeAdmin.dart';
import 'package:attendify/view/user/home/home.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AkhiriKegiatanService {
  final Dio _dio = Dio();
  final String baseUrl = "https://attendify.tech/mobile/api";

  Future<void> akhiriKegiatan(int id, String name, int userId, BuildContext context) async {
    try {
      showDialog(
        context: context, 
        builder: (context) {
          return Center(
            child: CircularProgressIndicator(
              color: Color(0xFF0C3569),
              backgroundColor: Color(0xFFE8E5E5),
            ),
          );
        }
      );
      final response = await _dio.put(
        '$baseUrl/kegiatan/$id', 
        data: {
          "name": name,
          "user_id": userId,
          "status": "0",
        }
      );


      Navigator.of(context).pop();
      
      if (response.statusCode == 200) {

        return showDialog(
          context: context,
          builder: (context) {
            return AlertDialog(
              content: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  // Image.asset(
                  //   'assets/success.gif',
                  //   height: 100,
                  //   width: 100,
                  // ),
                  Text(
                    "Kegiatan Berhasil Diakhiri", 
                    style: TextStyle(
                      fontSize: 20, 
                      fontWeight: 
                      FontWeight.bold
                    )
                  ),
                  
                ],
              ),
              actions: [
                Center(
                  child: Container(
                    margin: EdgeInsets.only(bottom: 10),
                    child: ElevatedButton(
                      onPressed: () async {

                        final LoginService _authService = LoginService();
                        Map<String, dynamic>? userData = await _authService.getUserData();
                        if (userData?['is_admin'] == '1') {
                          Navigator.pushReplacement(
                            context,
                            MaterialPageRoute(builder: (context) => HomeAdmin(userData: userData)),
                          );
                        } else {
                          Navigator.pushReplacement(
                            context,
                            MaterialPageRoute(builder: (context) => Home(userData: userData)),
                          );
                        }
                      },
                      style: ElevatedButton.styleFrom(
                        padding: EdgeInsets.symmetric(vertical: 10, horizontal: 60), 
                        primary: Color(0xFFF26139),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(15), // Atur radius sudut di sini
                        ),
                      ),
                      child: Text(
                        "Selesai",
                        style: TextStyle(
                          fontSize: 15,
                          color: Colors.white, 
                        ),
                      ),
                    ),
                  ),
                ),
                
              ],
            );
          },
        );
      } else {
        print('Gagal memperbarui status buku. Kode status: ${response.statusCode}');
      }
    } catch (error) {
      print('Terjadi kesalahan: $error');
    }
  }
}

