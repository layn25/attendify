import 'dart:convert';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LoginService {
  final Dio _dio = Dio();
  final String baseUrl = 'https://attendify.tech/mobile/api';

  Future<bool> login(String username, String password, BuildContext context) async {
    try {
      showDialog(
        context: context, 
        builder: (context) {
          return Center(
            child: CircularProgressIndicator(
              color: Color(0xFF0C3569),
              backgroundColor: Color(0xFFE8E5E5),
            ),
          );
        }
      );

      Response response = await _dio.get(
        '$baseUrl/akun',
        queryParameters: {'email': username}
      );

      Navigator.of(context).pop();
      if (response.statusCode == 200) {
        List<dynamic> userData = response.data['data'];
        List<dynamic> filteredUsers = userData.where((user) => user['email'] == username && user['password'] == password).toList();

        if (filteredUsers.isNotEmpty) {
          saveUserData(filteredUsers[0] as Map<String, dynamic>);
          return true;
        } else {
          return false;
        }
        } else {
        return false;
      }
    } catch (error) {
      print('Error during login: $error');
      return false;
    }
  }

  Future<void> saveUserData(Map<String, dynamic> userData) async {
    try {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      prefs.setString('user_data', json.encode(userData));
    } catch (error) {
      print('Error saving user data: $error');
    }
  }

  Future<Map<String, dynamic>?> getUserData() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? userDataString = prefs.getString('user_data');

    if (userDataString != null) {
      return json.decode(userDataString);
    } else {
      return null;
    }
  }

  Future<void> logout() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.remove('user_data');
  }





    // Future<void> saveUserId(int userId) async {
  //   try {
  //     SharedPreferences prefs = await SharedPreferences.getInstance();
  //     prefs.setInt('user_id', userId);
  //   } catch (error) {
  //     print('Error saving user ID: $error');
  //   }
  // }

  // Future<int?> getUserId() async {
  //   SharedPreferences prefs = await SharedPreferences.getInstance();
  //   // Mengambil ID pengguna dari SharedPreferences
  //   return prefs.getInt('user_id');
  // }

  // Future<void> logout() async {
  //   SharedPreferences prefs = await SharedPreferences.getInstance();
  //   prefs.remove('user_id');
  // }
}
