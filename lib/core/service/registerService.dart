import 'dart:convert';
import 'package:attendify/core/service/loginService.dart';
import 'package:attendify/view/admin/home/homeAdmin.dart';
import 'package:attendify/view/user/home/home.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';


class RegisterService {
  final Dio _dio = Dio();
  final String baseUrl = "https://attendify.tech/mobile/api";

  Future<void> register(String email, String password, String name, String nim, String whatsapp, BuildContext context) async {
    try {
      showDialog(
        context: context, 
        builder: (context) {
          return Center(
            child: CircularProgressIndicator(
              color: Color(0xFF0C3569),
              backgroundColor: Color(0xFFE8E5E5),
            ),
          );
        }
      );
      final response = await _dio.post(
        '$baseUrl/akun', 
        data: {
          "name": name,
          "nim": nim,
          "email": email,
          "password": password,
          "whatsapp": whatsapp,
          "is_admin": "0",
        }
      );

      Navigator.of(context).pop();
      
      if (response.statusCode == 200) {
        // List<dynamic> userData = response.data['data'];
        // List<dynamic> filteredUsers = userData.where((user) => user['email'] == email && user['password'] == password).toList();

        // saveUserData(filteredUsers[0] as Map<String, dynamic>);
        
        // final LoginService _authService = LoginService();
        // Map<String, dynamic>? userData = await _authService.getUserData();
        Map<String, dynamic> userData = response.data['data'];
          await saveUserData(userData);

          // Setelah menyimpan data akun, pindahkan pengguna ke halaman yang sesuai
          // Misalnya, ke halaman admin jika pengguna adalah admin
          if (userData['is_admin'] == '1') {
            Navigator.pushReplacement(
              context,
              MaterialPageRoute(builder: (context) => HomeAdmin(userData: userData)),
            );
          } else {
            Navigator.pushReplacement(
              context,
              MaterialPageRoute(builder: (context) => Home(userData: userData)),
            );
          }


      } else {
        print('Gagal memperbarui status buku. Kode status: ${response.statusCode}');
      }
    } catch (error) {
      print('Terjadi kesalahan: $error');
    }
  }
}

  Future<void> saveUserData(Map<String, dynamic> userData) async {
    try {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      prefs.setString('user_data', json.encode(userData));
    } catch (error) {
      print('Error saving user data: $error');
    }
  }