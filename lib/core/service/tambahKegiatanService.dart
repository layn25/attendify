import 'package:attendify/core/service/loginService.dart';
import 'package:attendify/view/admin/home/homeAdmin.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';

class TambahKegiatanService {
  final Dio _dio = Dio();
  final String baseUrl = "https://attendify.tech/mobile/api";

  final FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
      FlutterLocalNotificationsPlugin();

  TambahKegiatanService() {
    initNotifications();
  }

  Future<void> initNotifications() async {
    await flutterLocalNotificationsPlugin.initialize(
      InitializationSettings(
        android: AndroidInitializationSettings('mipmap/ic_launcher'),
      ),
    );
  }

  Future<void> tambahKegiatan(String name, int userId, BuildContext context) async {
    try {
      showDialog(
        context: context,
        builder: (context) {
          return Center(
            child: CircularProgressIndicator(
              color: Color(0xFF0C3569),
              backgroundColor: Color(0xFFE8E5E5),
            ),
          );
        }
      );

      final response = await _dio.post(
        '$baseUrl/kegiatan',
        data: {
          "name": name,
          "user_id": userId,
          "status": "1",
        }
      );

      Navigator.of(context).pop();

      if (response.statusCode == 200) {
        await showNotification(
          'Kegiatan Ditambahkan',
          'Kegiatan "$name" telah berhasil ditambahkan.',
        );

        final LoginService _authService = LoginService();
        Map<String, dynamic>? userData = await _authService.getUserData();
        Navigator.pushReplacement(
            context,
            MaterialPageRoute(
                builder: (context) => HomeAdmin(userData: userData)));
      } else {
        print('Gagal memperbarui status buku. Kode status: ${response.statusCode}');
      }
    } catch (error) {
      print('Terjadi kesalahan: $error');
    }
  }

  Future<void> showNotification(String title, String body) async {
    const AndroidNotificationDetails androidPlatformChannelSpecifics = AndroidNotificationDetails(
      'your_channel_id',
      'Your channel name',
      channelDescription: 'Your channel description',
      importance: Importance.max,
      priority: Priority.high,
      playSound: true,
    );

    const NotificationDetails platformChannelSpecifics = NotificationDetails(android: androidPlatformChannelSpecifics);

    await flutterLocalNotificationsPlugin.show(
      0,
      title,
      body,
      platformChannelSpecifics,
    );
  }
}