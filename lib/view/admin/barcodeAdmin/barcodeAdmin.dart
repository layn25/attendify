import 'package:attendify/const/const_color.dart';
import 'package:flutter/material.dart';
import 'package:qr_flutter/qr_flutter.dart';

class BarcodeAdmin extends StatefulWidget {
  const BarcodeAdmin({super.key});

  @override
  State<BarcodeAdmin> createState() => _BarcodeAdminState();
}

class _BarcodeAdminState extends State<BarcodeAdmin> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Container(
          decoration: BoxDecoration(color: COMPLEMENTARY_COLOR4),
          child: Stack(
            children: [
              AppBar(
                backgroundColor: TRANSPARENT_COLOR,
                elevation: 0.0,
                title: Text("Barcode Saya"),
              ),
              Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                      padding: EdgeInsets.all(14),
                      width: 350,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(20),
                        color: WHITE_COLOR,
                        boxShadow: [
                          BoxShadow(
                            color: SECONDARY_COLOR.withOpacity(0.1),
                            offset: Offset(0, -2),
                            blurRadius: 7,
                            spreadRadius: 0,
                          ),
                        ],
                      ),
                      child: Center(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Container(
                              margin: EdgeInsets.only(bottom: 5),
                              child: Text(
                                "QR Code Saya",
                                style: TextStyle(
                                  fontSize: 25,
                                  color: COMPLEMENTARY_COLOR6,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(top: 5, bottom: 10),
                              child: Text(
                                "Scan Barcode berikut",
                                style: TextStyle(
                                  fontSize: 18,
                                  color: ABU_COLOR,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ),
                            QrImageView(
                              data: "1234",
                              version: QrVersions.auto,
                              size: 320,
                              gapless: false,
                              errorStateBuilder: (cxt, err) {
                                return Container(
                                  child: Center(
                                    child: Text(
                                      'Uh oh! Something went wrong...',
                                      textAlign: TextAlign.center,
                                    ),
                                  ),
                                );
                              },
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
