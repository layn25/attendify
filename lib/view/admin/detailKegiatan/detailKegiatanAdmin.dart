import 'package:attendify/const/const_color.dart';
import 'package:attendify/core/model/kegiatan.dart';
import 'package:attendify/core/service/akhiriKegiatanService.dart';
import 'package:attendify/core/service/hadirKegiatanService.dart';
import 'package:attendify/view/admin/detailKegiatan/widget/listHadirWidget.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_barcode_scanner/flutter_barcode_scanner.dart';

class DetailKegiatanAdmin extends StatefulWidget {
  final int idKegiatan;

  const DetailKegiatanAdmin({Key? key, required this.idKegiatan})
      : super(key: key);

  @override
  State<DetailKegiatanAdmin> createState() => _DetailKegiatanAdminState();
}

class _DetailKegiatanAdminState extends State<DetailKegiatanAdmin> {
  final KegiatanList kegiatanList = KegiatanList();
  String _scanBarcode = '';

  Future<void> scanQR() async {
    var barcodeScanRes;
    try {
      barcodeScanRes = await FlutterBarcodeScanner.scanBarcode(
        '#ff6666',
        'Cancel',
        true,
        ScanMode.QR,
      );
      if (barcodeScanRes != null && barcodeScanRes != "-1") {
        int iduser = int.parse(barcodeScanRes);
        final hadirKegiatanService = HadirKegiatanService();
        hadirKegiatanService.hadirKegiatan(iduser, widget.idKegiatan, context);
      }
    } on PlatformException {
      barcodeScanRes = 'Failed to get platform version.';
    }

    if (!mounted) return;

    setState(() {
      _scanBarcode = barcodeScanRes;
    });
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<List<Kegiatan>>(
      stream: kegiatanList.fetchKegiatan().map((kegiatanList) {
        return kegiatanList
            .where((kegiatan) => kegiatan.id == widget.idKegiatan)
            .toList();
      }),
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.waiting) {
          return Center(child: CircularProgressIndicator());
        } else {
          var kegiatan = snapshot.data;
          return Scaffold(
            body: Container(
              decoration: BoxDecoration(color: COMPLEMENTARY_COLOR6),
              child: Stack(
                children: [
                  AppBar(
                    backgroundColor: TRANSPARENT_COLOR,
                    elevation: 0.0,
                    leading: IconButton(
                      icon: const Icon(
                        Icons.arrow_back,
                        color: WHITE_COLOR,
                      ),
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                    ),
                    title: Text(
                      "List Kehadiran",
                      style: TextStyle(
                        color: WHITE_COLOR,
                      ),
                    ),
                  ),
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(top: 80),
                        child: Column(
                          children: [
                            Container(
                              margin: EdgeInsets.only(top: 20),
                              height: 150,
                              width: 150,
                              child: Material(
                                color: Colors.transparent,
                                borderRadius: BorderRadius.circular(12),
                                elevation: 5,
                                child: Container(
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(12),
                                    gradient: LinearGradient(
                                      colors: [
                                        QR_COLOR,
                                        QR_COLOR
                                      ], // Sesuaikan dengan warna yang diinginkan
                                      begin: Alignment.topLeft,
                                      end: Alignment.bottomRight,
                                    ),
                                  ),
                                  child: InkWell(
                                    borderRadius: BorderRadius.circular(12),
                                    onTap: () {
                                      if (kegiatan![0].status == "1") {
                                        scanQR();
                                      } else {
                                        showDialog(
                                          context: context,
                                          builder: (context) {
                                            return AlertDialog(
                                              title: Text("Tidak Bisa Scan"),
                                              content: Text(
                                                  "maaf kegiatan telah berakhir"),
                                              actions: [
                                                TextButton(
                                                  onPressed: () {
                                                    Navigator.of(context).pop();
                                                  },
                                                  child: Text("Keluar"),
                                                  style: TextButton.styleFrom(
                                                    primary:
                                                        COMPLEMENTARY_COLOR6,
                                                  ),
                                                ),
                                              ],
                                            );
                                          },
                                        );
                                      }
                                    },
                                    child: Center(
                                      child: Icon(
                                        Icons.qr_code_2_outlined,
                                        color: WHITE_COLOR,
                                        size: 100,
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(top: 20),
                              child: Text(
                                kegiatan![0].name,
                                style: TextStyle(
                                  fontSize: 24,
                                  color: WHITE_COLOR,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                      Expanded(
                        child: Container(
                          margin: EdgeInsets.only(top: 20),
                          padding: EdgeInsets.all(5),
                          width: MediaQuery.of(context).size.width,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(20.0),
                              topRight: Radius.circular(20.0),
                            ),
                            color: WHITE_COLOR,
                            boxShadow: [
                              BoxShadow(
                                color: SECONDARY_COLOR.withOpacity(0.1),
                                offset: Offset(0, -2),
                                blurRadius: 7,
                                spreadRadius: 0,
                              ),
                            ],
                          ),
                          child: SingleChildScrollView(
                            child: Column(
                              children: [
                                ListHadirWidget(idKegiatan: widget.idKegiatan),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            bottomNavigationBar: Visibility(
              visible: (kegiatan[0].status == "1"),
              child: InkWell(
                onTap: () {
                  final akhiriKegiatanService = AkhiriKegiatanService();
                  akhiriKegiatanService.akhiriKegiatan(
                    kegiatan[0].id,
                    kegiatan[0].name,
                    kegiatan[0].user_id,
                    context,
                  );
                },
                child: Container(
                  color: FAILED_COLOR,
                  padding: EdgeInsets.all(10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        "Akhiri Kegiatan",
                        style: TextStyle(
                          color: WHITE_COLOR,
                          fontSize: 18,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          );
        }
      },
    );
  }
}
