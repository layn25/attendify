import 'package:flutter/material.dart';
import 'package:attendify/const/const_color.dart';
import 'package:attendify/core/model/kegiatan.dart';

class EditSemuaKegiatan extends StatefulWidget {
  final Kegiatan kegiatan;
  final Function(Kegiatan) onSave;

  EditSemuaKegiatan({required this.kegiatan, required this.onSave});

  @override
  _EditSemuaKegiatanState createState() => _EditSemuaKegiatanState();
}

class _EditSemuaKegiatanState extends State<EditSemuaKegiatan> {
  TextEditingController nameController = TextEditingController();
  TextEditingController statusController = TextEditingController();

  @override
  void initState() {
    super.initState();
    nameController.text = widget.kegiatan.name;
    statusController.text = widget.kegiatan.status;
  }

  void saveChanges() {
    String updatedName = nameController.text;
    String updatedStatus = statusController.text;

    if (updatedName.isNotEmpty && updatedStatus.isNotEmpty) {
      Kegiatan updatedKegiatan = Kegiatan(
        widget.kegiatan.id,
        updatedName,
        widget.kegiatan.user_id,
        updatedStatus,
      );

      widget.onSave(updatedKegiatan);

      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Text('Changes saved successfully'),
          duration: Duration(seconds: 2),
        ),
      );

      Navigator.of(context).pop();
    } else {
      showDialog(
        context: context,
        builder: (context) => AlertDialog(
          title: Text('Error'),
          content: Text('Please fill in all the fields.'),
          actions: [
            ElevatedButton(
              onPressed: () {
                Navigator.of(context).pop();
              },
              child: Text('OK'),
            ),
          ],
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(color: COMPLEMENTARY_COLOR6),
        child: Stack(
          children: [
            AppBar(
              backgroundColor: Colors.transparent,
              elevation: 0.0,
              leading: IconButton(
                icon: const Icon(
                  Icons.arrow_back,
                  color: Colors.white,
                ),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
              title: Text(
                'Edit Kegiatan',
                style: TextStyle(
                  color: Colors.white,
                ),
              ),
            ),
            Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    padding: EdgeInsets.all(14),
                    width: 350,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(20),
                      color: Colors.white,
                      boxShadow: [
                        BoxShadow(
                          color: Colors.grey.withOpacity(0.1),
                          offset: Offset(0, -2),
                          blurRadius: 7,
                          spreadRadius: 0,
                        ),
                      ],
                    ),
                    child: Center(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Container(
                            margin: EdgeInsets.only(top: 20, bottom: 20),
                            child: Text(
                              'Edit Kegiatan',
                              style: TextStyle(
                                fontSize: 35,
                                color: COMPLEMENTARY_COLOR6,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(bottom: 20),
                            height: 55,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(20),
                              border: Border.all(
                                  color: COMPLEMENTARY_COLOR6, width: 2.0),
                            ),
                            child: TextField(
                              style: TextStyle(
                                fontSize: 20,
                                color: Colors.grey[800],
                              ),
                              decoration: InputDecoration(
                                filled: true,
                                fillColor: Colors.white,
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(20),
                                  borderSide: BorderSide.none,
                                ),
                                hintText: 'Kegiatan Name',
                                hintStyle: TextStyle(
                                  color: Colors.grey[400],
                                  fontWeight: FontWeight.w400,
                                  fontSize: 17,
                                ),
                                contentPadding: EdgeInsets.symmetric(
                                    horizontal: 22, vertical: 18),
                              ),
                              controller: nameController,
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(bottom: 20),
                            height: 55,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(20),
                              border: Border.all(
                                  color: COMPLEMENTARY_COLOR6, width: 2.0),
                            ),
                            child: TextField(
                              style: TextStyle(
                                fontSize: 20,
                                color: Colors.grey[800],
                              ),
                              decoration: InputDecoration(
                                filled: true,
                                fillColor: Colors.white,
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(20),
                                  borderSide: BorderSide.none,
                                ),
                                hintText: 'Kegiatan Status',
                                hintStyle: TextStyle(
                                  color: Colors.grey[400],
                                  fontWeight: FontWeight.w400,
                                  fontSize: 17,
                                ),
                                contentPadding: EdgeInsets.symmetric(
                                    horizontal: 22, vertical: 18),
                              ),
                              controller: statusController,
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(top: 12, bottom: 20),
                            child: ElevatedButton(
                              onPressed: saveChanges,
                              style: ElevatedButton.styleFrom(
                                padding: EdgeInsets.symmetric(
                                    vertical: 15, horizontal: 70),
                                primary: COMPLEMENTARY_COLOR6,
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(20),
                                ),
                              ),
                              child: Text(
                                'Save Changes',
                                style: TextStyle(
                                  fontSize: 18,
                                  color: Colors.white,
                                ),
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
