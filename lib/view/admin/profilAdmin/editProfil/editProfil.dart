import 'package:attendify/core/service/editProfilServoce.dart';
import 'package:flutter/material.dart';
import 'package:attendify/const/const_color.dart';

class EditProfil extends StatefulWidget {
  final Map<String, dynamic>? userData;

  const EditProfil({Key? key, this.userData}) : super(key: key);

  @override
  _EditProfilState createState() => _EditProfilState();
}

class _EditProfilState extends State<EditProfil> {
  final _namaController = TextEditingController();
  final _nimController = TextEditingController();
  final _whatsappController = TextEditingController();

  @override
  void initState() {
    super.initState();
    _namaController.text = widget.userData?['name'];
    _nimController.text = widget.userData?['nim'];
    _whatsappController.text = widget.userData?['whatsapp'];
  }

  // void _saveChanges() {
  //   String updatedName = _namaController.text;
  //   String updatedNIM = _nimController.text;
  //   String updatedWhatsapp = _whatsappController.text;

  //   if (updatedName.isNotEmpty &&
  //       updatedNIM.isNotEmpty &&
  //       updatedWhatsapp.isNotEmpty) {
  //     widget.userData?['name'] = updatedName;
  //     widget.userData?['nim'] = updatedNIM;
  //     widget.userData?['whatsapp'] = updatedWhatsapp;

  //     Navigator.of(context).pop(widget.userData);
  //   } else {
  //     showDialog(
  //       context: context,
  //       builder: (context) => AlertDialog(
  //         title: Text('Error'),
  //         content: Text('Please fill in all the fields.'),
  //         actions: [
  //           ElevatedButton(
  //             onPressed: () {
  //               Navigator.of(context).pop();
  //             },
  //             child: Text('OK'),
  //           ),
  //         ],
  //       ),
  //     );
  //   }
  // }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Container(
        decoration: BoxDecoration(color: COMPLEMENTARY_COLOR6),
        child: Stack(
          children: [
            AppBar(
              backgroundColor: TRANSPARENT_COLOR,
              elevation: 0.0,
              leading: IconButton(
                icon: const Icon(
                  Icons.arrow_back,
                  color: WHITE_COLOR,
                ),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
              title: Text(
                "Edit Profil",
                style: TextStyle(
                  color: WHITE_COLOR,
                ),
              ),
            ),
            Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    padding: EdgeInsets.all(14),
                    width: 350,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(20),
                      color: WHITE_COLOR,
                      boxShadow: [
                        BoxShadow(
                          color: SECONDARY_COLOR.withOpacity(0.1),
                          offset: Offset(0, -2),
                          blurRadius: 7,
                          spreadRadius: 0,
                        ),
                      ],
                    ),
                    child: Center(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Container(
                            margin: EdgeInsets.only(top: 20, bottom: 20),
                            child: Text(
                              "Edit Profil",
                              style: TextStyle(
                                fontSize: 35,
                                color: COMPLEMENTARY_COLOR6,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(bottom: 20),
                            height: 55,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(20),
                              border: Border.all(
                                  color: COMPLEMENTARY_COLOR6, width: 2.0),
                            ),
                            child: TextField(
                              style: TextStyle(
                                fontSize: 20,
                                color: SECONDARY_COLOR,
                              ),
                              decoration: InputDecoration(
                                filled: true,
                                fillColor: WHITE_COLOR,
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(20),
                                  borderSide: BorderSide.none,
                                ),
                                hintText: "Masukkan Nama",
                                hintStyle: TextStyle(
                                  color: ABU_COLOR[400],
                                  fontWeight: FontWeight.w400,
                                  fontSize: 17,
                                ),
                                contentPadding: EdgeInsets.symmetric(
                                    horizontal: 22, vertical: 18),
                              ),
                              controller: _namaController,
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(bottom: 20),
                            height: 55,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(20),
                              border: Border.all(
                                  color: COMPLEMENTARY_COLOR6, width: 2.0),
                            ),
                            child: TextField(
                              style: TextStyle(
                                fontSize: 20,
                                color: SECONDARY_COLOR,
                              ),
                              decoration: InputDecoration(
                                filled: true,
                                fillColor: WHITE_COLOR,
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(20),
                                  borderSide: BorderSide.none,
                                ),
                                hintText: "Masukkan NIM",
                                hintStyle: TextStyle(
                                  color: ABU_COLOR[400],
                                  fontWeight: FontWeight.w400,
                                  fontSize: 17,
                                ),
                                contentPadding: EdgeInsets.symmetric(
                                    horizontal: 22, vertical: 18),
                              ),
                              controller: _nimController,
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(bottom: 20),
                            height: 55,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(20),
                              border: Border.all(
                                  color: COMPLEMENTARY_COLOR6, width: 2.0),
                            ),
                            child: TextField(
                              style: TextStyle(
                                fontSize: 20,
                                color: SECONDARY_COLOR,
                              ),
                              decoration: InputDecoration(
                                filled: true,
                                fillColor: WHITE_COLOR,
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(20),
                                  borderSide: BorderSide.none,
                                ),
                                hintText: "Masukkan Whatsapp",
                                hintStyle: TextStyle(
                                  color: ABU_COLOR[400],
                                  fontWeight: FontWeight.w400,
                                  fontSize: 17,
                                ),
                                contentPadding: EdgeInsets.symmetric(
                                    horizontal: 22, vertical: 18),
                              ),
                              controller: _whatsappController,
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(top: 12, bottom: 20),
                            child: ElevatedButton(
                              onPressed: () {
                                final editProfilService = EditProfilService();
                                editProfilService.editProfil(
                                  widget.userData?['id'], 
                                  widget.userData?['email'], 
                                  widget.userData?['password'], 
                                  _namaController.text.trim(), 
                                  _nimController.text.trim(), 
                                  _whatsappController.text.trim(),
                                  widget.userData?['is_admin'],
                                  context
                                );
                              },
                              style: ElevatedButton.styleFrom(
                                padding: EdgeInsets.symmetric(
                                    vertical: 15, horizontal: 70),
                                primary: COMPLEMENTARY_COLOR6,
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(20),
                                ),
                              ),
                              child: Text(
                                "Simpan Perubahan",
                                style: TextStyle(
                                  fontSize: 18,
                                  color: WHITE_COLOR,
                                ),
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
