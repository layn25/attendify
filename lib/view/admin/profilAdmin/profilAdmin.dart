// ProfilPage.dart

import 'package:attendify/const/const_color.dart';
import 'package:attendify/view/admin/profilAdmin/editProfil/editProfil.dart';
import 'package:flutter/material.dart';

class ProfilPage extends StatefulWidget {
  final Map<String, dynamic>? userData;

  ProfilPage({Key? key, this.userData}) : super(key: key);

  @override
  _ProfilPageState createState() => _ProfilPageState();
}

class _ProfilPageState extends State<ProfilPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Profil',
          style: TextStyle(color: Colors.white),
        ),
        backgroundColor: COMPLEMENTARY_COLOR6,
        iconTheme: IconThemeData(color: Colors.white),
      ),
      body: Container(
        color: COMPLEMENTARY_COLOR6,
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Card(
            elevation: 5,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(15),
            ),
            color: WHITE_COLOR,
            child: Padding(
              padding: const EdgeInsets.all(16.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Center(
                    child: CircleAvatar(
                      radius: 50,
                      backgroundImage: AssetImage('assets/logo/user.png'),
                    ),
                  ),
                  if (widget.userData != null) ...[
                    SizedBox(height: 10),
                    ListTile(
                      title: Text(
                        'Name',
                        style: TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      subtitle: Text(
                        widget.userData?['name'],
                        style: TextStyle(fontSize: 16),
                      ),
                    ),
                    ListTile(
                      title: Text(
                        'NIM',
                        style: TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      subtitle: Text(
                        widget.userData?['nim'],
                        style: TextStyle(fontSize: 16),
                      ),
                    ),
                    ListTile(
                      title: Text(
                        'Whatsapp',
                        style: TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      subtitle: Text(
                        widget.userData?['whatsapp'],
                        style: TextStyle(fontSize: 16),
                      ),
                    ),
                  ],
                  SizedBox(
                      height: 10), // Reduced space between profile and button
                  ElevatedButton.icon(
                    onPressed: () async {
                      // Navigate to the EditProfil page and wait for the result
                      var updatedUserData = await Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) =>
                              EditProfil(userData: widget.userData),
                        ),
                      );

                      // Check if there are updated user data
                      if (updatedUserData != null) {
                        // Update the userData in ProfilPage
                        setState(() {
                          widget.userData?['name'] = updatedUserData['name'];
                        });
                      }
                    },
                    icon: Icon(
                      Icons.edit,
                      color: Colors.white, // Set the icon color to white
                    ),
                    label: Text(
                      'Edit Profil',
                      style: TextStyle(color: WHITE_COLOR),
                    ),
                    style: ElevatedButton.styleFrom(
                      primary: COMPLEMENTARY_COLOR6,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(20),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
