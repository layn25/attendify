import 'package:attendify/const/const_color.dart';
import 'package:flutter/material.dart';
import 'package:attendify/core/model/kegiatan.dart';
import 'package:attendify/view/admin/editSemuaKegiatan/editSemuaKegiatan.dart';

class SemuaKegiatan extends StatefulWidget {
  final Map<String, dynamic>? userData;

  SemuaKegiatan({Key? key, this.userData}) : super(key: key);
  @override
  _SemuaKegiatanState createState() => _SemuaKegiatanState();
}

class _SemuaKegiatanState extends State<SemuaKegiatan> {
  final KegiatanList kegiatanList = KegiatanList();
  TextEditingController searchController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0.0,
        leading: IconButton(
          icon: const Icon(
            Icons.arrow_back,
            color: WHITE_COLOR,
          ),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
        title: Text(
          'Semua Kegiatan',
          style: TextStyle(color: Colors.white),
        ),
        backgroundColor: COMPLEMENTARY_COLOR6,
      ),
      body: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: TextField(
              controller: searchController,
              onChanged: (value) {
                // Implement your search logic here
                // For example: kegiatanList.filter((kegiatan) => kegiatan.name.contains(value));
              },
              decoration: InputDecoration(
                labelText: 'Search Kegiatan',
                prefixIcon: Icon(Icons.search),
                border: OutlineInputBorder(),
              ),
            ),
          ),
          Expanded(
            child: StreamBuilder<List<Kegiatan>>(
              stream: kegiatanList.fetchKegiatan().map((kegiatanList) {
                return kegiatanList.where((kegiatan) =>kegiatan.user_id ==  widget.userData?['id']).toList();
              }),
              builder: (context, snapshot) {
                if (snapshot.connectionState == ConnectionState.waiting) {
                  return Center(child: CircularProgressIndicator());
                } else if (snapshot.hasError) {
                  return Center(child: Text('Error: ${snapshot.error}'));
                } else {
                  var kegiatan = snapshot.data;
                  return ListView.builder(
                    itemCount: kegiatan!.length,
                    itemBuilder: (context, index) {
                      return InkWell(
                        onTap: () {
                          // Implement navigation to detail page
                          // For example: Navigator.push(context, MaterialPageRoute(builder: (context) => DetailKegiatanAdmin(idKegiatan: kegiatan[index].id)));
                        },
                        child: Container(
                          margin: EdgeInsets.all(8.0),
                          height: 80,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(20),
                            color: Colors.white,
                            boxShadow: [
                              BoxShadow(
                                color: Colors.grey.withOpacity(0.1),
                                offset: Offset(0, -2),
                                blurRadius: 7,
                                spreadRadius: 0,
                              ),
                            ],
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Row(
                                children: [
                                  Container(
                                    height: 50,
                                    width: 50,
                                    decoration: BoxDecoration(
                                      image: DecorationImage(
                                        image: AssetImage('assets/logo.png'),
                                        fit: BoxFit.cover,
                                      ),
                                    ),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(left: 12, top: 10),
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          kegiatan[index].name,
                                          style: TextStyle(
                                            color: Colors.black,
                                            fontWeight: FontWeight.bold,
                                            fontSize: 18,
                                          ),
                                          overflow: TextOverflow.ellipsis,
                                          maxLines: 1,
                                        ),
                                        Text(
                                          (kegiatan[index].status == "1")
                                              ? "Active"
                                              : "Tidak Active",
                                          style: TextStyle(
                                            color:
                                                (kegiatan[index].status == "1")
                                                    ? Colors.green
                                                    : Colors.red,
                                            fontWeight: FontWeight.normal,
                                            fontSize: 16,
                                          ),
                                        )
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                              IconButton(
                                icon: Icon(
                                  Icons.edit,
                                  color: Colors.blue,
                                ),
                                onPressed: () {
                                  // Handle edit button press
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                      builder: (context) => EditSemuaKegiatan(
                                        kegiatan: kegiatan[index],
                                        onSave: (Kegiatan) {},
                                      ),
                                    ),
                                  );
                                },
                              ),
                            ],
                          ),
                        ),
                      );
                    },
                  );
                }
              },
            ),
          ),
        ],
      ),
    );
  }
}
