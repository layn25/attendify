import 'package:attendify/const/const_color.dart';
import 'package:attendify/core/service/tambahKegiatanService.dart';
import 'package:flutter/material.dart';

class TambahKegiatan extends StatefulWidget {
  final Map<String, dynamic>? userData;
  const TambahKegiatan({super.key, this.userData});

  @override
  State<TambahKegiatan> createState() => _TambahKegiatanState();
}

class _TambahKegiatanState extends State<TambahKegiatan> {
  final _namaController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Container(
        decoration: BoxDecoration(color: COMPLEMENTARY_COLOR6),
        child: Stack(
          children: [
            AppBar(
              backgroundColor: TRANSPARENT_COLOR,
              elevation: 0.0,
              leading: IconButton(
                icon: const Icon(
                  Icons.arrow_back,
                  color: WHITE_COLOR,
                ),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
              title: Text(
                "Tambah Kegiatan",
                style: TextStyle(
                  color: WHITE_COLOR,
                ),
              ),
            ),
            Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    padding: EdgeInsets.all(14),
                    width: 350,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(20),
                      color: WHITE_COLOR,
                      boxShadow: [
                        BoxShadow(
                          color: SECONDARY_COLOR.withOpacity(0.1),
                          offset: Offset(0, -2),
                          blurRadius: 7,
                          spreadRadius: 0,
                        ),
                      ],
                    ),
                    child: Center(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Container(
                            margin: EdgeInsets.only(top: 20, bottom: 20),
                            child: Text(
                              "Tambah Kegiatan",
                              style: TextStyle(
                                fontSize: 35,
                                color: COMPLEMENTARY_COLOR6,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(bottom: 20),
                            height: 55,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(20),
                              border: Border.all(
                                  color: COMPLEMENTARY_COLOR6, width: 2.0),
                            ),
                            child: TextField(
                              style: TextStyle(
                                fontSize: 20,
                                color: SECONDARY_COLOR,
                              ),
                              decoration: InputDecoration(
                                filled: true,
                                fillColor: WHITE_COLOR,
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(20),
                                  borderSide: BorderSide.none,
                                ),
                                hintText: "Masukan Nama Kegiatan",
                                hintStyle: TextStyle(
                                  color: ABU_COLOR[400],
                                  fontWeight: FontWeight.w400,
                                  fontSize: 17,
                                ),
                                contentPadding: EdgeInsets.symmetric(
                                    horizontal: 22, vertical: 18),
                              ),
                              controller: _namaController
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(top: 12, bottom: 20),
                            child: ElevatedButton(
                              onPressed: () {
                                final tambahKegiatanService = TambahKegiatanService();
                                tambahKegiatanService.tambahKegiatan(
                                  _namaController.text.trim(), 
                                  widget.userData?['id'], 
                                  context
                                );

                              },
                              style: ElevatedButton.styleFrom(
                                padding: EdgeInsets.symmetric(
                                    vertical: 15, horizontal: 70),
                                primary: COMPLEMENTARY_COLOR6,
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(
                                      20), // Atur radius sudut di sini
                                ),
                              ),
                              child: Text(
                                "Tambah",
                                style: TextStyle(
                                  fontSize: 18,
                                  color: WHITE_COLOR,
                                ),
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
