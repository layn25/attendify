import 'package:attendify/const/const_color.dart';
import 'package:attendify/core/service/loginService.dart';
import 'package:attendify/view/admin/home/homeAdmin.dart';
import 'package:attendify/view/user/home/home.dart';
import 'package:flutter/material.dart';
import 'package:attendify/view/register/register.dart';


class Login extends StatefulWidget {
  const Login({super.key});

  @override
  State<Login> createState() => _LoginState();
}


class _LoginState extends State<Login> {
  final LoginService _authService = LoginService();
  final _emailController = TextEditingController();
  final _passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: Color(0xffF2FAFF),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(children: [
            Center(
              child: Container(
                margin: EdgeInsets.only(top: 64, bottom: 14),
                height: 190,
                child: Image.asset(
                  "assets/logo.png",
                  fit: BoxFit.contain,
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 10),
              child: Text(
                "ATTENDIFY",
                style: TextStyle(
                  fontSize: 24,
                  color: Color(0xFF5C229A),
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 14, bottom: 14),
              padding: EdgeInsets.all(20),
              child: Column(
                children: [
                  Container(
                    height: 55,
                    child: TextField(
                        decoration: InputDecoration(
                          filled: true,
                          fillColor: Colors.white,
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(20),
                            borderSide: BorderSide.none,
                          ),
                          hintText: "Email",
                          hintStyle: TextStyle(
                            color: Colors.grey[400],
                            fontWeight: FontWeight.w400,
                            fontSize: 17,
                          ),
                          contentPadding: EdgeInsets.symmetric(
                              horizontal: 22, vertical: 18),
                        ),
                        controller: _emailController),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 14, bottom: 14),
                    height: 55,
                    child: TextField(
                      decoration: InputDecoration(
                        filled: true,
                        fillColor: Colors.white,
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(20),
                          borderSide: BorderSide.none,
                        ),
                        hintText: "Password",
                        hintStyle: TextStyle(
                          color: Colors.grey[400],
                          fontWeight: FontWeight.w400,
                          fontSize: 17,
                        ),
                        contentPadding:
                            EdgeInsets.symmetric(horizontal: 22, vertical: 18),
                      ),
                      obscureText: true,
                      controller: _passwordController,
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 12),
                    child: ElevatedButton(
                      onPressed: () async {
                        bool success = await _authService.login(
                        _emailController.text,
                        _passwordController.text,
                        context,
                        );
                        
                        if (success) {
                          Map<String, dynamic>? userData = await _authService.getUserData();
                          if (userData?['email'] != _emailController.text) {
                            bool success = await _authService.login(
                              _emailController.text,
                              _passwordController.text,
                              context,
                            );
                            
                            if (success) {
                              Map<String, dynamic>? userData = await _authService.getUserData();
                              if (userData?['is_admin'] == '1') {
                                Navigator.pushReplacement(
                                  context,
                                  MaterialPageRoute(builder: (context) => HomeAdmin(userData: userData)),
                                );
                              } else {
                                Navigator.pushReplacement(
                                  context,
                                  MaterialPageRoute(builder: (context) => Home(userData: userData)),
                                );
                              }
                            } else {
                              // Tampilkan pesan kesalahan
                              ScaffoldMessenger.of(context).showSnackBar(
                                SnackBar(
                                  content: Text('Gagal login. Periksa kembali email dan password Anda.'),
                                ),
                              );
                            }
                          } else if (userData?['is_admin'] == '1') {
                            Navigator.pushReplacement(
                              context,
                              MaterialPageRoute(builder: (context) => HomeAdmin(userData: userData)),
                            );
                          } else {
                            Navigator.pushReplacement(
                              context,
                              MaterialPageRoute(builder: (context) => Home(userData: userData)),
                            );
                          }
                        } else {
                          // Tampilkan pesan kesalahan
                          ScaffoldMessenger.of(context).showSnackBar(
                            SnackBar(
                              content: Text('Gagal login. Periksa kembali email dan password Anda.'),
                            ),
                          );
                        }


                      },
                      style: ElevatedButton.styleFrom(
                        padding:
                            EdgeInsets.symmetric(vertical: 15, horizontal: 70),
                        primary: COMPLEMENTARY_COLOR6,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(
                              20), // Atur radius sudut di sini
                        ),
                      ),
                      child: Text(
                        "Login",
                        style: TextStyle(
                          fontSize: 18,
                          color: Colors.white,
                        ),
                      ),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 20),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Container(
                          margin: EdgeInsets.only(top: 10),
                          child: Text(
                            "Belum memiliki akun?",
                            style: TextStyle(
                              fontSize: 17,
                              color: Color(0xFFF26139),
                              fontWeight: FontWeight.w400,
                            ),
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(top: 10),
                          child: TextButton(
                            onPressed: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => register()),
                              );
                            },
                            child: Text(
                              "Daftar Sekarang",
                              style: TextStyle(
                                fontSize: 17,
                                color: COMPLEMENTARY_COLOR6,
                                fontWeight: FontWeight.w400,
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ),
          ]),
        ),
      ),
    );
  }
}
