import 'package:attendify/core/service/registerService.dart';
import 'package:flutter/material.dart';
import 'package:attendify/view/login/login.dart';

class register extends StatefulWidget {
  const register({super.key});

  @override
  State<register> createState() => _LoginState();
}

final _emailController = TextEditingController();
final _passwordController = TextEditingController();
final _nameController = TextEditingController();
final _nimController = TextEditingController();
final _whatsappController = TextEditingController();

class _LoginState extends State<register> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: Color(0xffF2FAFF),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(children: [
            Center(
              child: Container(
                margin: EdgeInsets.only(top: 24, bottom: 14),
                height: 150,
                child: Image.asset(
                  "assets/logo.png",
                  fit: BoxFit.contain,
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 10),
              child: Text(
                "ATTENDIFY",
                style: TextStyle(
                  fontSize: 24,
                  color: Color(0xFF5C229A),
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 14, bottom: 14),
              padding: EdgeInsets.all(20),
              child: Column(
                children: [
                  Container(
                    height: 55,
                    child: TextField(
                      decoration: InputDecoration(
                        filled: true,
                        fillColor: Colors.white,
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(20),
                          borderSide: BorderSide.none,
                        ),
                        hintText: "Email",
                        hintStyle: TextStyle(
                          color: Colors.grey[400],
                          fontWeight: FontWeight.w400,
                          fontSize: 17,
                        ),
                        contentPadding: EdgeInsets.symmetric(
                            horizontal: 22, vertical: 18),
                      ),
                      controller: _emailController
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 14, bottom: 14),
                    height: 55,
                    child: TextField(
                      decoration: InputDecoration(
                        filled: true,
                        fillColor: Colors.white,
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(20),
                          borderSide: BorderSide.none,
                        ),
                        hintText: "Password",
                        hintStyle: TextStyle(
                          color: Colors.grey[400],
                          fontWeight: FontWeight.w400,
                          fontSize: 17,
                        ),
                        contentPadding:
                            EdgeInsets.symmetric(horizontal: 22, vertical: 18),
                      ),
                      obscureText: true,
                      controller: _passwordController,
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(bottom: 14),
                    height: 55,
                    child: TextField(
                      decoration: InputDecoration(
                        filled: true,
                        fillColor: Colors.white,
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(20),
                          borderSide: BorderSide.none,
                        ),
                        hintText: "Name",
                        hintStyle: TextStyle(
                          color: Colors.grey[400],
                          fontWeight: FontWeight.w400,
                          fontSize: 17,
                        ),
                        contentPadding: EdgeInsets.symmetric(
                            horizontal: 22, vertical: 18),
                      ),
                      controller: _nameController
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(bottom: 14),
                    height: 55,
                    child: TextField(
                      decoration: InputDecoration(
                        filled: true,
                        fillColor: Colors.white,
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(20),
                          borderSide: BorderSide.none,
                        ),
                        hintText: "Nim",
                        hintStyle: TextStyle(
                          color: Colors.grey[400],
                          fontWeight: FontWeight.w400,
                          fontSize: 17,
                        ),
                        contentPadding: EdgeInsets.symmetric(
                            horizontal: 22, vertical: 18),
                      ),
                      controller: _nimController
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(bottom: 14),
                    height: 55,
                    child: TextField(
                      decoration: InputDecoration(
                        filled: true,
                        fillColor: Colors.white,
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(20),
                          borderSide: BorderSide.none,
                        ),
                        hintText: "Whatsapp",
                        hintStyle: TextStyle(
                          color: Colors.grey[400],
                          fontWeight: FontWeight.w400,
                          fontSize: 17,
                        ),
                        contentPadding: EdgeInsets.symmetric(
                            horizontal: 22, vertical: 18),
                      ),
                      controller: _whatsappController
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 12),
                    child: ElevatedButton(
                      onPressed: () {
                        final registerService = RegisterService();
                        registerService.register(
                          _emailController.text.trim(), 
                          _passwordController.text.trim(), 
                          _nameController.text.trim(), 
                          _nimController.text.trim(), 
                          _whatsappController.text.trim(), 
                          context
                        );
                      },
                      style: ElevatedButton.styleFrom(
                        padding:
                            EdgeInsets.symmetric(vertical: 15, horizontal: 70),
                        primary: Color(0xFF5C229A),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(20), 
                        ),
                      ),
                      child: Text(
                        "Register",
                        style: TextStyle(
                          fontSize: 18,
                          color: Colors.white,
                        ),
                      ),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 20),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Container(
                          margin: EdgeInsets.only(top: 10),
                          child: Text(
                            "Sudah memiliki akun?",
                            style: TextStyle(
                              fontSize: 17,
                              color: Color(0xFFF26139),
                              fontWeight: FontWeight.w400,
                            ),
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(top: 10),
                          child: TextButton(
                            onPressed: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => Login()),
                              );
                            },
                            child: Text(
                              "Login Sekarang",
                              style: TextStyle(
                                fontSize: 17,
                                color: Color(0xFF473ED7),
                                fontWeight: FontWeight.w400,
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ),
          ]),
        ),
      ),
    );
  }
}
