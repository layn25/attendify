import 'package:attendify/core/service/loginService.dart';
import 'package:attendify/view/admin/home/homeAdmin.dart';
import 'package:attendify/view/login/login.dart';
import 'package:attendify/view/user/home/home.dart';
import 'package:flutter/material.dart';

class Verifikasi extends StatefulWidget {
  const Verifikasi({super.key});

  @override
  State<Verifikasi> createState() => _VerifikasiState();
}

class _VerifikasiState extends State<Verifikasi> {
  final LoginService _authService = LoginService();
  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: _authService.getUserData(),
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.waiting) {
          return CircularProgressIndicator();
        } else if (snapshot.hasData) {
          Map<String, dynamic> userData = snapshot.data as Map<String, dynamic>;

          if (userData['is_admin'] == '1') {
            return HomeAdmin(userData: userData);
          } else {
            return Home(userData: userData);
          }
        } else {
          // Jika data pengguna tidak tersedia, tampilkan halaman login
          return Login();
        }
      },
    );
  }
}