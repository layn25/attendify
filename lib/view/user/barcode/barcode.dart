import 'package:attendify/const/const_color.dart';
import 'package:flutter/material.dart';
import 'package:qr_flutter/qr_flutter.dart';

class Barcode extends StatefulWidget {
  final Map<String, dynamic>? userData;
  const Barcode({super.key, this.userData});

  @override
  State<Barcode> createState() => _BarcodeState();
}

class _BarcodeState extends State<Barcode> {
  @override
  Widget build(BuildContext context) {
    String iduser = widget.userData!['id'].toString();
    return SafeArea(
      child: Scaffold(
        body: Container(
          decoration: BoxDecoration(color: COMPLEMENTARY_COLOR6),
          child: Stack(
            children: [
              AppBar(
                backgroundColor: Colors.transparent,
                elevation: 0.0,
                leading: IconButton(
                  icon: const Icon(
                    Icons.arrow_back,
                    color: WHITE_COLOR,
                  ),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
                title: Text(
                  "Barcode Saya",
                  style: TextStyle(
                    color: WHITE_COLOR,
                  ),
                ),
              ),
              Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                      padding: EdgeInsets.all(14),
                      width: 350,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(20),
                        color: WHITE_COLOR,
                        boxShadow: [
                          BoxShadow(
                            color: Colors.black.withOpacity(0.1),
                            offset: Offset(0, -2),
                            blurRadius: 7,
                            spreadRadius: 0,
                          ),
                        ],
                      ),
                      child: Center(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Container(
                              margin: EdgeInsets.only(bottom: 5),
                              child: Text(
                                "QR Code Saya",
                                style: TextStyle(
                                  fontSize: 25,
                                  color: COMPLEMENTARY_COLOR6,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(top: 5, bottom: 10),
                              child: Text(
                                "Scan Barcode berikut",
                                style: TextStyle(
                                  fontSize: 18,
                                  color: Color(0xFF666666),
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ),
                            QrImageView(
                              data: iduser,
                              version: QrVersions.auto,
                              size: 320,
                              gapless: false,
                              errorStateBuilder: (cxt, err) {
                                return Container(
                                  child: Center(
                                    child: Text(
                                      'Uh oh! Something went wrong...',
                                      textAlign: TextAlign.center,
                                    ),
                                  ),
                                );
                              },
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
