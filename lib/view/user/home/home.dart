import 'package:attendify/const/const_color.dart';
import 'package:attendify/core/model/kegiatan.dart';
import 'package:attendify/core/model/userkegiatan.dart';
import 'package:attendify/core/service/loginService.dart';
import 'package:attendify/view/admin/profilAdmin/profilAdmin.dart';
import 'package:attendify/view/login/login.dart';
import 'package:attendify/view/user/barcode/barcode.dart';
import 'package:attendify/view/user/home/widget/kegiatanWidget.dart';
import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

class Home extends StatefulWidget {
  final Map<String, dynamic>? userData;
  const Home({super.key, this.userData});

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  final LoginService _authService = LoginService();
  final KegiatanList kegiatanList = KegiatanList();
  final UserKegiatanList userkegiatanList = UserKegiatanList();

  Future<void> _refreshData() async {
    await Future.delayed(Duration(seconds: 1));
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xffF2FAFF),
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0.0,
        title: Row(
          children: [
            Container(
              margin: EdgeInsets.only(top: 5),
              height: 50,
              width: 50,
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                color: Colors.blue,
                image: DecorationImage(
                  image: AssetImage('assets/logo/user.png'),
                  fit: BoxFit.cover,
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 5, left: 10),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                      constraints: BoxConstraints(maxWidth: 250),
                      child: Text(
                        "Halo, ${widget.userData?['name']}",
                        style: TextStyle(
                          fontSize: 18,
                          color: Color(0xFF0C3569),
                          fontWeight: FontWeight.normal,
                        ),
                        overflow: TextOverflow.ellipsis,
                        maxLines: 1,
                      )),
                  Container(
                    child: Text(
                      "Selamat datang",
                      style: TextStyle(color: Color(0xFF0C3569)),
                    ),
                  )
                ],
              ),
            ),
          ],
        ),
        automaticallyImplyLeading: false,
        actions: [
          IconButton(
            icon: const Icon(
              Icons.logout,
              color: COMPLEMENTARY_COLOR6,
            ),
            onPressed: () async {
              showDialog(
                context: context,
                builder: (context) {
                  return AlertDialog(
                    title: Text("Keluar"),
                    content: Text("Apakah Anda Ingin Keluar dari Akun ini"),
                    actions: [
                      TextButton(
                        onPressed: () async {
                          Navigator.of(context).pop(); // Menutup dialog
                          await _authService.logout();
                          Navigator.pushReplacement(
                            context,
                            MaterialPageRoute(builder: (context) => Login()),
                          );
                        },
                        child: Text("Iya"),
                        style: TextButton.styleFrom(
                          primary: FAILED_COLOR,
                        ),
                      ),
                      TextButton(
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                        child: Text("Tidak"),
                        style: TextButton.styleFrom(
                          primary: COMPLEMENTARY_COLOR6,
                        ),
                      ),
                    ],
                  );
                },
              );
            },
          ),
        ],
      ),
      body: RefreshIndicator(
        onRefresh: _refreshData,
        child: SingleChildScrollView(
          physics: AlwaysScrollableScrollPhysics(),
          child: Center(
            child: Column(
              children: [
                Container(
                  margin: EdgeInsets.only(top: 20),
                  height: 115,
                  width: 350,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20),
                    gradient: LinearGradient(
                      begin: Alignment.topLeft,
                      end: Alignment.bottomRight,
                      colors: [
                        Color(0xFF0C3569), // Warna pertama
                        Color(0xFF1CCAF0), // Warna kedua
                      ],
                    ),
                    boxShadow: [
                      BoxShadow(
                        color: SECONDARY_COLOR.withOpacity(0.1),
                        offset: Offset(0, -2),
                        blurRadius: 7,
                        spreadRadius: 0,
                      ),
                    ],
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        margin: EdgeInsets.only(top: 14, left: 15),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                              child: Text("Total Kegiatan",
                                  style: TextStyle(
                                    color: Color(0xFFFFFFFF),
                                    fontWeight: FontWeight.normal,
                                    fontSize: 20,
                                  )),
                            ),
                            StreamBuilder<List<UserKegiatan>>(
                              stream: userkegiatanList.fetchUserKegiatan().map((kegiatanList) {
                                return kegiatanList.where((kegiatan) => kegiatan.user_id == widget.userData!['id']).toList();
                              }),
                              builder: (context, snapshot) {
                                if (snapshot.connectionState == ConnectionState.waiting) {
                                  return Container(
                                    height: 50,
                                    width: 50,
                                    child: Shimmer.fromColors(
                                      baseColor: Colors.grey.shade300,
                                      highlightColor: Colors.grey.shade100,
                                      enabled: true,
                                      child: Container(                                                           
                                        decoration: BoxDecoration(
                                          color: Colors.grey,
                                          borderRadius: BorderRadius.circular(40),
                                        ),
                                        
                                      ),
                                    ),
                                  );
                                } else {
                                  var kegiatan = snapshot.data;
                                  return Container(
                                    margin: EdgeInsets.only(top: 5),
                                    child: Text(" ${kegiatan!.length} Kegiatan",
                                        style: TextStyle(
                                          color: WHITE_COLOR,
                                          fontWeight: FontWeight.bold,
                                          fontSize: 30,
                                        )),
                                  );
                                }
                              }
                            ),
                          ],
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(right: 5),
                        child: IconButton(
                          icon: const Icon(
                            Icons.qr_code_2_outlined,
                            color: Color(0xFFffffff),
                          ),
                          iconSize: 100,
                          onPressed: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => Barcode(userData: widget.userData)
                              )
                            );
                          },
                        ),
                      )
                    ],
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(right: 5, left: 5),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        margin: EdgeInsets.only(top: 14, left: 20),
                        child: Text("Daftar Kegiatan",
                            style: TextStyle(
                              color: Color(0xFF5C229A),
                              fontWeight: FontWeight.bold,
                              fontSize: 20,
                            )),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 14, right: 20),
                        child: Text("Lihat Semua",
                            style: TextStyle(
                              color: Color(0xFF5C229A),
                              fontWeight: FontWeight.normal,
                              fontSize: 18,
                            )),
                      ),
                    ],
                  ),
                ),
                MatakuliahWidget(userData: widget.userData)
              ],
            ),
          ),
        ),
      ),
      bottomNavigationBar: Container(
        height: 60,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(20),
          color: Color(0xFFFFFFFF),
          boxShadow: [
            BoxShadow(
              color: Colors.black.withOpacity(0.1),
              offset: Offset(0, -4),
              blurRadius: 4,
              spreadRadius: 0,
            ),
          ],
        ),
        child: BottomNavigationBar(
          backgroundColor: Colors.transparent,
          elevation: 0,
          selectedFontSize: 12,
          unselectedFontSize: 12,
          selectedItemColor: Color(0xFF5C229A),
          unselectedItemColor: Color(0xFF5C229A).withOpacity(0.6),
          items: [
            BottomNavigationBarItem(
              icon: Icon(Icons.home),
              label: 'Home',
            ),
            BottomNavigationBarItem(
              icon: GestureDetector(
                onTap: () {
                  // Navigate to the ProfilPage when the "Profil" icon is clicked
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) =>
                            ProfilPage(userData: widget.userData)),
                  );
                },
                child: Icon(Icons.account_circle_outlined),
              ),
              label: 'Profil',
            )
          ],
        ),
      ),
    );
  }
}
