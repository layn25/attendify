import 'package:attendify/const/const_color.dart';
import 'package:attendify/core/model/kegiatan.dart';
import 'package:attendify/core/model/userkegiatan.dart';
import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

class MatakuliahWidget extends StatelessWidget {
  final Map<String, dynamic>? userData;
  const MatakuliahWidget({super.key, this.userData});

  @override
  Widget build(BuildContext context) {
    final UserKegiatanList userkegiatanList = UserKegiatanList();
    final KegiatanList kegiatanList = KegiatanList();
    return Container(
      child: SingleChildScrollView(
        child: StreamBuilder<List<UserKegiatan>>(
          stream: userkegiatanList.fetchUserKegiatan().map((kegiatanList) {
            return kegiatanList.where((kegiatan) => kegiatan.user_id == userData?['id']).toList();
          }),
          builder: (context, snapshot) {
            if (snapshot.connectionState == ConnectionState.waiting) {
              // return Center(child: CircularProgressIndicator());
              return Column(
                children: [
                  for (int i = 0; i < 10 ; i++)
                  Container(
                    margin: EdgeInsets.only(left: 20, top: 14, right: 20 ),
                    height: 80,
                    width: 350,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(20),
                      color: Color(0xFFFFFFFF),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.black.withOpacity(0.1),
                          offset: Offset(0, -2),
                          blurRadius: 7,
                          spreadRadius: 0,
                        ),
                      ],
                    ),
                    child: Container(
                      margin: EdgeInsets.only(left: 14),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Row(
                            children: [
                              Container(
                                height: 50,
                                width: 50,
                                child: Shimmer.fromColors(
                                  baseColor: Colors.grey.shade300,
                                  highlightColor: Colors.grey.shade100,
                                  enabled: true,
                                  child: Container(                                                           
                                    decoration: BoxDecoration(
                                      color: Colors.grey,
                                      borderRadius: BorderRadius.circular(40),
                                    ),
                                    
                                  ),
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.only(left: 12, top: 15),
                                child:Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Container(
                                      margin: EdgeInsets.only(bottom: 5),
                                      constraints: BoxConstraints(maxWidth: 180),
                                      child: Shimmer.fromColors(
                                        baseColor: Colors.grey.shade300,
                                        highlightColor: Colors.grey.shade100,
                                        enabled: true,
                                        child: Container(     
                                          height: 20,                                                           
                                          decoration: BoxDecoration(
                                            color: Colors.grey,
                                            borderRadius: BorderRadius.circular(8),
                                          ),
                                        ),
                                      ),
                                    ),
                                    Container(
                                      margin: EdgeInsets.only(bottom: 14),
                                      constraints: BoxConstraints(maxWidth: 100),
                                      child: Shimmer.fromColors(
                                        baseColor: Colors.grey.shade300,
                                        highlightColor: Colors.grey.shade100,
                                        enabled: true,
                                        child: Container(     
                                          height: 20,                                                           
                                          decoration: BoxDecoration(
                                            color: Colors.grey,
                                            borderRadius: BorderRadius.circular(8),
                                          ),
                                          
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            ],
                          ),
                          Container(
                            margin: EdgeInsets.only(right: 15),
                            child: Container(
                              height: 50,
                              width: 50,
                              child: Shimmer.fromColors(
                                baseColor: Colors.grey.shade300,
                                highlightColor: Colors.grey.shade100,
                                enabled: true,
                                child: Container(                                                           
                                  decoration: BoxDecoration(
                                    color: Colors.grey,
                                    borderRadius: BorderRadius.circular(40),
                                  ),
                                  
                                ),
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ],
              );
            } else if (snapshot.hasError) {
              return Center(child: Text('Error: ${snapshot.error}'));
            } else {
              var userkegiatan = snapshot.data;
              return Column(
                children: [
                  for (int i = 0; i <userkegiatan!.length ; i++)
                    InkWell(
                      onTap: () {
                        // Navigator.push(context, MaterialPageRoute(builder: (context) => DetailBuku(buku: buku[i])));
                      },
                      child: Container(
                        margin: EdgeInsets.only(left: 20, top: 14, right: 20),
                        height: 80,
                        width: 350,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(20),
                          color: Color(0xFFFFFFFF),
                          boxShadow: [
                            BoxShadow(
                              color: Colors.black.withOpacity(0.1),
                              offset: Offset(0, -2),
                              blurRadius: 7,
                              spreadRadius: 0,
                            ),
                          ],
                        ),
                        child: Container(
                          margin: EdgeInsets.only(left: 14),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Row(
                                children: [
                                  Container(
                                    height: 50,
                                    width: 50,
                                    decoration: BoxDecoration(
                                      image: DecorationImage(
                                        image: AssetImage('assets/logo.png'),
                                        fit: BoxFit.cover,
                                      ),
                                    ),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(left: 12, top: 10),
                                    child: Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        StreamBuilder<List<Kegiatan>>(
                                          stream: kegiatanList.fetchKegiatan().map((kegiatanList) {
                                            return kegiatanList.where((kegiatan) => kegiatan.id == userkegiatan[i].kegiatan_id).toList();
                                          }),
                                          builder: (context, snapshot) {
                                            if (snapshot.connectionState == ConnectionState.waiting) {
                                              return Container(
                                                height: 20,
                                                width: 50,
                                                child: Shimmer.fromColors(
                                                  baseColor: Colors.grey.shade300,
                                                  highlightColor: Colors.grey.shade100,
                                                  enabled: true,
                                                  child: Container(                                                           
                                                    decoration: BoxDecoration(
                                                      color: Colors.grey,
                                                      borderRadius: BorderRadius.circular(0),
                                                    ),
                                                    
                                                  ),
                                                ),
                                              );
                                            } else {
                                              var kegiatan = snapshot.data;
                                              return Container(
                                                margin: EdgeInsets.only(bottom: 5),
                                                constraints: BoxConstraints(maxWidth: 140),
                                                child: Text(
                                                  kegiatan![0].name,
                                                  style: TextStyle(
                                                    color: Color(0xFF0C3569),
                                                    fontWeight: FontWeight.bold,
                                                    fontSize: 18,
                                                  ),
                                                  overflow: TextOverflow.ellipsis,
                                                  maxLines: 1,
                                                ),
                                              );
                                            }
                                          }
                                        ),
                                        Container(
                                          margin: EdgeInsets.only(bottom: 10),
                                          child: Text(
                                            "Hadir",
                                            style: TextStyle(
                                              color: SUCCESS_COLOR,
                                              fontWeight: FontWeight.normal,
                                              fontSize: 16,
                                            ),
                                          ),
                                        )
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                ],
              );
            }
          }
        ),
      ),
    );
  }
}
